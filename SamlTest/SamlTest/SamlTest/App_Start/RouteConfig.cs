﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SamlTest
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Saml2",
                url: "Saml2/{id}",
                defaults: new { controller = "Saml2", action = "SignIn", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "SPA",
                url: "{*url}",
                defaults: new { controller = "Home", action = "Index" }
            );

            
        }
    }
}
