﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using SamlTest.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SamlTest.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        // Only needed while signing with mvc
        [HttpPost]
        [AllowAnonymous]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Home", new { ReturnUrl = returnUrl }));
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            var claims = loginInfo.ExternalIdentity.Claims.ToList();
            ViewData["claims"] = claims;
            ViewData["isAuthenticated"] = loginInfo.ExternalIdentity.IsAuthenticated;

            var token = GenerateLocalAccessTokenResponse(claims);
            ViewData["user_access_token"] = token;

            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            return View();
        }

        private string GenerateLocalAccessTokenResponse(List<Claim> claims)
        {
            var tokenExpiration = TimeSpan.FromDays(1);

            ClaimsIdentity identity = new ClaimsIdentity(OAuthDefaults.AuthenticationType);

            identity.AddClaim(new Claim(ClaimTypes.Name, "Inderjeet"));

            foreach (var item in claims)
            {
                identity.AddClaim(new Claim(item.Type, item.Value));
            }

            var props = new AuthenticationProperties()
            {
                IssuedUtc = DateTime.UtcNow,
                ExpiresUtc = DateTime.UtcNow.Add(tokenExpiration),
            };

            var ticket = new AuthenticationTicket(identity, props);

            var accessToken = Startup.OAuthOptions.AccessTokenFormat.Protect(ticket);

            return accessToken;
        }
    }
}
